from setuptools import setup
from os import path

# Get the long description from the README file
with open(path.join(path.dirname(__file__), 'Readme.rst'), 'r') as f:
    long_description = f.read()

setup(
    name='gitlab-release',
    use_scm_version=True,  # This will generate the version number from git tags

    description='Utility for use in gitlab ci to upload files (from build) to the current projects release (tag)',
    long_description=long_description,
    url='https://gitlab.com/alelec/gitlab-release',
    author='Andrew Leech',
    author_email='andrew@alelec.net',
    license='MIT',
    py_modules=['gitlab_release'],
    setup_requires=['setuptools_scm'],
    install_requires=['requests'],

    # To provide executable scripts, use entry points keyword. This should point to a function.
    # Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    entry_points={
        'console_scripts': [
            'gitlab_release=gitlab_release:main',
        ],
    },
)
