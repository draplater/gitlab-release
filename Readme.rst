gitlab\_release
===============

This python utility is used to upload specific release
files from a gitlab-ci build to the tag release page on gitlab.

This allows a file or (generated) zip with arbitrary structure to be added to 
a release for convenient download, as opposed to the artifacts which are 
somewhat constrained to always be zipped with a folder structure taken 
directly from the filesystem.

It can be used in a gitlab-ci.yml stage like:

::

    release:
      stage: release
      image: python3
      script:
        - pip3 install gitlab_release
        - python3 -m gitlab_release "$PRIVATE_TOKEN" *.zip
      only:
        - tags

Or if you want to bundle a bunch of files into a zip and upload that

::

    release:
      stage: release
      image: python3
      script:
        - pip3 install gitlab_release
        - python3 -m gitlab_release --zip "release-${CI_COMMIT_TAG}.zip" "$PRIVATE_TOKEN" *
      only:
        - tags

This tool requires you to put a copy of a PRIVATE\_TOKEN in a
secret CI variable for the project to give it access to post the files back to the project

You can use your personal token from https://gitlab.com/profile/account

Or better, create a new Personal Access Token for this and use that:
https://gitlab.com/profile/personal\_access\_tokens

Make sure you enable API access when creating the token. The label you use is 
just for you to remeber what it's for, it wont affeect operation.

Add it to a secret variable called PRIVATE\_TOKEN for the project you’re
building at https://gitlab.com/\<user\>/\<project\>/settings/ci\_cd

On any tagged release the files specified in the glob will then be
availble on the Tags tab of your project.

Usage
-----

::

    usage: gitlab_release [-h] [--server SERVER] [--project_id PROJECT_ID]
                         [--release_tag RELEASE_TAG] [--timeout TIMEOUT]
                         [--ignore_cert] [--zip ZIP]
                         [--description DESCRIPTION]
                         private_token files [files ...]


========================= ===== 
positional arguments         
========================= ===== 
private_token             login token with permissions to commit to repo
files                     file names and/or globs of files to upload
========================= ===== 


========================= ===== 
optional arguments           
========================= ===== 
--server SERVER           url of gitlab server or $CI_PROJECT_URL
--project_id PROJECT_ID   Unique id of project, available in Project Settings/General or $CI_PROJECT_ID
--release_tag RELEASE_TAG Tag to upload files against or $CI_BUILD_TAG
--timeout TIMEOUT         Timeout for http requests
--ignore_cert             Ignore ssl certificate failures
--zip ZIP                 Add all globbed files to provided zip name and upload that
--description DESCRIPTION Extra text to add to the release description
-h, --help                show this help message and exit
========================= ===== 

