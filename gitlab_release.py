#!/usr/bin/env python3

import os
import io
import json
import codecs
import zipfile
import argparse
import datetime
import itertools
import mimetypes
import urllib.error
from glob import glob
from urllib.request import Request, urlopen
from urllib.parse import urljoin, urlencode, quote
import requests

class MultiPartForm(object):
    """Accumulate the data to be used when posting a form."""

    def __init__(self):
        self.form_fields = []
        self.files = []
        # self.boundary = mimetools.choose_boundary()
        self.boundary = '----------lImIt_of_THE_fIle_eW_$'
        return

    def get_content_type(self):
        return 'multipart/form-data; boundary=%s' % self.boundary

    def add_field(self, name, value):
        """Add a simple field to the form data."""
        self.form_fields.append((name, value))
        return

    def add_file(self, fieldname, filename, fileHandle, mimetype=None):
        """Add a file to be uploaded."""
        body = fileHandle.read()
        if mimetype is None:
            mimetype = mimetypes.guess_type(filename)[0] or 'application/octet-stream'
        self.files.append((fieldname, filename, mimetype, body))
        return

    def get_binary(self):
        """Return a binary buffer containing the form data, including attached files."""
        def to_bytes(s):
            return s.encode('ascii') if isinstance(s, str) else s

        part_boundary = '--' + self.boundary

        binary = io.BytesIO()
        needsCLRF = False
        # Add the form fields
        for name, value in self.form_fields:
            if needsCLRF:
                binary.write('\r\n')
            needsCLRF = True

            block = [part_boundary,
              'Content-Disposition: form-data; name="%s"' % name,
              '',
              value
            ]
            binary.write('\r\n'.join(block))

        # Add the files to upload
        for field_name, filename, content_type, body in self.files:
            if needsCLRF:
                binary.write('\r\n')
            needsCLRF = True

            block = [part_boundary,
              str('Content-Disposition: file; name="%s"; filename="%s"' % \
              (field_name, filename)),
              'Content-Type: %s' % content_type,
              ''
              ]
            binary.write(b'\r\n'.join([to_bytes(s) for s in block]))
            binary.write(b'\r\n')
            binary.write(to_bytes(body))

        # add closing boundary marker,
        binary.write(to_bytes('\r\n--' + self.boundary + '--\r\n'))
        return binary


def check_requests_error(rsp, desc, raise_=True):
    try:
        rsp.raise_for_status()
    except Exception as e:
        print("{} failed, error: {}".format(desc, e))
        if raise_:
            raise
        else:
            return e


def main():
    parser = argparse.ArgumentParser(description='Upload files to gitlab tag (release)')
    parser.add_argument('--server', default=None, help='url of gitlab server or $CI_PROJECT_URL')
    parser.add_argument('--project_id', default=None, help='Unique id of project, available in '
                                                           'Project Settings/General or $CI_PROJECT_ID')
    parser.add_argument('--release_tag', help='Tag to upload files against or $CI_BUILD_TAG, or create new tag.')
    parser.add_argument('--ref', help='ref or $CI_COMMIT_SHA')
    parser.add_argument('--timeout', type=int, default=120, help='Timeout for http requests')
    parser.add_argument('--ignore_cert', action="store_true", help='Ignore ssl certificate failures')
    parser.add_argument('--zip', help='Add all globbed files to provided zip name and upload that')
    parser.add_argument('--description', help='Extra text to add to the release description or use $CI_COMMIT_MESSAGE')
    parser.add_argument('--name', help='name of release')
    parser.add_argument('private_token', help='login token with permissions to commit to repo')
    parser.add_argument('files', nargs="+", help='glob/s of files to upload')

    args = parser.parse_args()

    server = args.server or os.environ['CI_PROJECT_URL']
    if not server:
        print("Must provide --server if not running from CI")
        exit(1)
    if not server.endswith('/'):
        server += '/'

    auth = {'PRIVATE-TOKEN': args.private_token}
    verify = not args.ignore_cert

    project_id = args.project_id or os.environ['CI_PROJECT_ID']

    # gitlab may be hosted under a relative url
    api_url = urljoin(server, "../../api/v4/projects/%s/" % project_id)

    if not project_id:
        print("Must provide --project_id if not running from CI")
        exit(1)
    project_id = quote(project_id, safe='')

    ref = args.ref or os.environ['CI_COMMIT_SHA']

    release_tag = args.release_tag or os.environ.get('CI_COMMIT_TAG')
    if not release_tag:
        release_tag = datetime.datetime.now().strftime("%Y%m%d.%H%M%S.%f")[:-3]
        tag_url = urljoin(api_url, 'repository/tags')
        data = {"tag_name": release_tag, "ref": ref}
        rsp = requests.post(tag_url, json=data, headers=auth, verify=verify)
        check_requests_error(rsp, "create new tag")

    name = args.name or release_tag
    description = args.description or os.environ.get('CI_COMMIT_MESSAGE', "")

    print("Uploading to %s (id: %s) @ %s" % (server, project_id, release_tag))
    all_files = list(itertools.chain(*[glob(f) for f in args.files]))
    
    if args.zip:
        with zipfile.ZipFile(args.zip, "w", zipfile.ZIP_DEFLATED) as zf:
            def zipdir(path, ziph):
                # ziph is zipfile handle
                for root, dirs, files in os.walk(path):
                    for file in files:
                        ziph.write(os.path.join(root, file))

            for fname in all_files:
                print (fname)
                if fname == args.zip:
                    continue
                if os.path.isdir(fname):
                    zipdir(fname, zf)
                else:
                    zf.write(fname)

        all_files = [os.path.abspath(args.zip)]
        
    print("Uploading %s" % all_files)
    uploads = []
    for fname in all_files:
        with codecs.open(fname, 'rb') as filehandle:
            rsp = requests.post(urljoin(api_url, 'uploads'), files={'file': filehandle}, headers=auth, verify=verify)
            try:
                rsp.raise_for_status()
            except Exception as ex:
                print("Upload of {f} failed: {ex}".format(f=fname, ex=ex))
            else:
                uploads.append(rsp.json()['markdown'])

    # Now we've got the uploaded file info, attach that to the tag
    url = urljoin(api_url, 'releases')
    data  = {'description': description + "\n\n" + "\n".join(uploads),
             "name": name, "ref": ref, "tag_name": release_tag}
    rsp = requests.post(url, json=data, headers=auth, verify=verify)
    if rsp.status_code == 409:
        rsp = requests.delete(urljoin(url + "/", release_tag),headers=auth)
        try:
            rsp.raise_for_status()
        except Exception as e:
            print("Delete old release failed, error: {}".format(e))

        rsp = requests.post(url, json=data, headers=auth, verify=verify)
    try:
        rsp.raise_for_status()
    except Exception as ex:
        print("Setting tag description failed: \"{d}\" error: {ex}".format(d=data, ex=ex))


if __name__ == '__main__':
    main()